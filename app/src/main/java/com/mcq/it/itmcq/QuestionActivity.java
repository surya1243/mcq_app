package com.mcq.it.itmcq;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.mcq.it.itmcq.adapter.AnswerAdapter;
import com.mcq.it.itmcq.app.AppController;
import com.mcq.it.itmcq.exceptions.BadNetworkResponseException;
import com.mcq.it.itmcq.model.Question;
import com.mcq.it.itmcq.model.User;
import com.mcq.it.itmcq.parser.QuestionResponseParser;
import com.mcq.it.itmcq.parser.UserResponseParser;
import com.mcq.it.itmcq.remote.Server;
import com.mcq.it.itmcq.utils.SnackBarDisplay;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QuestionActivity extends AppCompatActivity {

    private String tagName =QuestionActivity.class.getSimpleName();
    private View.OnClickListener onNextButtonClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateViewForQuestion(questionsList.get(questionIndex<(questionsList.size()-1)?++questionIndex:questionIndex));
        }
    };
    private View.OnClickListener onPreviousButtonClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateViewForQuestion(questionsList.get(questionIndex>0?--questionIndex:questionIndex));
        }
    };
    private List<Question> questionsList;
    private int questionIndex=0;
    private TextView txt_question;
    private ImageView btn_previous,btn_next;
    private RecyclerView answers_show_recycler_view;
    private AnswerAdapter answerAdapter;

    private List<String> selected_tags;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        selected_tags=Arrays.asList(getIntent().getStringExtra(SearchPage.KEY_SELECTED_TAGS));
        //Initialize view components
        txt_question =findViewById(R.id.txt_question);
        btn_next=findViewById(R.id.btn_next);
        btn_previous=findViewById(R.id.btn_previous);
        answers_show_recycler_view=findViewById(R.id.answers_show_recycler_view);
        btn_next.setOnClickListener(onNextButtonClicked);
        btn_previous.setOnClickListener(onPreviousButtonClicked);

        fetchQuestionAsync();
    }


    private void updateViewForQuestion(Question newQuestion)
    {
        txt_question.setText(newQuestion.getQuestionString());
        answerAdapter= new AnswerAdapter(newQuestion);
        answers_show_recycler_view.setAdapter(answerAdapter);

    }
    void fetchQuestionAsync()
    {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait fetching questions...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        String url = new Server().getServerUrl() + "/api/question";
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                QuestionResponseParser questionResponseParser= new QuestionResponseParser();
                try{
                    questionsList =questionResponseParser.parseResponseQuestions(new JSONArray(response));
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                    showQuiz();

                }
                catch(BadNetworkResponseException e){
                    Log.d(tagName, e.getLocalizedMessage());
                    progress.dismiss();
                    SnackBarDisplay.showDefaultSnackBar(answers_show_recycler_view, e.getMessage());
                }
                catch(JSONException e){
                    Log.d(tagName, e.getLocalizedMessage());
                    progress.dismiss();
                    SnackBarDisplay.showDefaultSnackBar(answers_show_recycler_view, e.getMessage());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(tagName, error.getLocalizedMessage());
                progress.dismiss();
                SnackBarDisplay.showDefaultSnackBar(answers_show_recycler_view, "network error");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String submitStr=TextUtils.join(",", selected_tags);
                String lastChar=submitStr.substring(submitStr.length() - 1);
                if(lastChar.equals(","))
                    submitStr=submitStr.substring(0, submitStr.length() - 1);
                params.put("tags", submitStr);
                Toast.makeText(getApplicationContext(),TextUtils.join(",", selected_tags),Toast.LENGTH_LONG).show();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                // params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr);

    }

    private void showQuiz()
    {

    }
}

