package com.mcq.it.itmcq.customViews;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class ErrorDialogView extends DialogFragment {
    public static ErrorDialogView newInstance(String message) {
        ErrorDialogView f = new ErrorDialogView();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("message", message);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String message;
        if (getArguments() != null)
        {
            message= getArguments().getString("message");
        }
        else
            message="Error occurred";

        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(message);
        builder.setTitle("Oops..\nSomething went wrong.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dismiss();
            }
        });
        // Create the AlertDialog object and return it
        return builder.create();

    }
}

