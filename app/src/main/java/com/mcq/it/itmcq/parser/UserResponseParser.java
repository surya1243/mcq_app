package com.mcq.it.itmcq.parser;

import com.mcq.it.itmcq.exceptions.BadNetworkResponseException;
import com.mcq.it.itmcq.model.User;

import org.json.JSONException;
import org.json.JSONObject;

public class UserResponseParser {


    public User parseResponse(JSONObject rawObject) throws JSONException,BadNetworkResponseException
    {

        if(!rawObject.getBoolean("status"))
        {
            throw new BadNetworkResponseException(rawObject.getString("message"));
        }

        User user = new User();
        JSONObject rawUser =rawObject.getJSONObject("data");
        user.firstName =rawUser.getString("firstName");
        user.lastName=rawUser.getString("lastName");
        user.email=rawUser.getString("email");
        user.address=rawUser.getString("address");
        return user;

    }
}
