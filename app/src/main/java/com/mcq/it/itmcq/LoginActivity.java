package com.mcq.it.itmcq;


import android.app.ProgressDialog;
import android.content.Intent;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.mcq.it.itmcq.app.AppController;
import com.mcq.it.itmcq.customViews.ErrorDialogView;
import com.mcq.it.itmcq.exceptions.BadNetworkResponseException;
import com.mcq.it.itmcq.model.User;
import com.mcq.it.itmcq.parser.UserResponseParser;
import com.mcq.it.itmcq.remote.Server;
import com.mcq.it.itmcq.utils.SnackBarDisplay;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;



/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {
String tagName =LoginActivity.class.getSimpleName();
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView =  findViewById(R.id.email);

        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }


    private void attemptLogin()
    {
        String email =mEmailView.getText().toString();
        String password =mPasswordView.getText().toString();
        makeLoginAsyncToServer(email,password);

    }

    private void onSuccessLogin()
    {
        //TODO when login is success
        Intent i = new Intent(getApplicationContext(),SearchPage.class);
        startActivity(i);

    }
    private void onNoSuccessLogin(String noSuccessMessage)
    {
        //TODO when login is not success
        showErrorDialog(noSuccessMessage);
        mPasswordView.getText().clear();
        mEmailView.getText().clear();
    }

    void showErrorDialog(String message) {


        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentManager fragmentManager = getSupportFragmentManager();
        // Create and show the dialog.
        DialogFragment newFragment = ErrorDialogView.newInstance(message);

        newFragment.show(fragmentManager,"dialog");

    }
    public void makeLoginAsyncToServer(final String email,final String password) {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait Performing login...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        String url = new Server().getServerUrl() + "/api/login";
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                UserResponseParser userResponseParser= new UserResponseParser();
                try{
                    User user =userResponseParser.parseResponse(new JSONObject(response));
                    //TODO save User to database

                    onSuccessLogin();
                    progress.dismiss();

                }
                catch(BadNetworkResponseException e){
                    Log.d(tagName, e.getLocalizedMessage());
                    progress.dismiss();
                    onNoSuccessLogin(e.getMessage());


                }
                catch(JSONException e){
                    Log.d(tagName, e.getLocalizedMessage());
                    progress.dismiss();
                    onNoSuccessLogin(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(tagName, error.getLocalizedMessage());
                progress.dismiss();
                SnackBarDisplay.showDefaultSnackBar(mPasswordView, "network error");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // Removed this line if you dont need it or Use application/json
                // params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr);
    }


}



