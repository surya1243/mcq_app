package com.mcq.it.itmcq.exceptions;


public class BadNetworkResponseException extends Exception {
    public BadNetworkResponseException(){
        super("Bad network response");
    }

    public BadNetworkResponseException(String message)
    {super(message);}

}
