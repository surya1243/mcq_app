package com.mcq.it.itmcq.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mcq.it.itmcq.R;
import com.mcq.it.itmcq.model.Question;

import java.util.Collections;
import java.util.List;

public class AnswerAdapter  extends RecyclerView.Adapter<AnswerAdapter.MyViewHolder> {
    private List<String> answers;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txt_answer,txt_answer_count;
        public MyViewHolder(View v) {
            super(v);
            txt_answer = v.findViewById(R.id.txt_answer);
            txt_answer_count=v.findViewById(R.id.txt_answer_count);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AnswerAdapter(Question question) {
        this.answers=question.getIncorrectAnswers();
        this.answers.add(question.getCorrectAnswer());

        //shuffle answers
        Collections.shuffle(this.answers);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AnswerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.answer_adapter_layout, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.txt_answer_count.setText(getChar(getItemCount()));
        holder.txt_answer.setText(this.answers.get(getItemCount()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.answers.size();
    }
    public static char getChar(int i) {
        return i<0 || i>25 ? '?' : (char)('A' + i);
    }
}