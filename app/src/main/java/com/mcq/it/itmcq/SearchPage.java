package com.mcq.it.itmcq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;


import com.mcq.it.itmcq.utils.SnackBarDisplay;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;

public class SearchPage extends AppCompatActivity {

public static String KEY_SELECTED_TAGS ="SELECTED_TAGS";
    MultiSelectSpinner multiSelectSpinner;
    Button button;
    List<String> options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_page);
        multiSelectSpinner =findViewById(R.id.multiselectSpinner);
        button =findViewById(R.id.btn_submit);
        options=Arrays.asList(getResources().getStringArray(R.array.tags));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(SearchPage.this, android.R.layout.simple_list_item_multiple_choice, options);
        multiSelectSpinner.setItems(options);
        multiSelectSpinner.setListAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean[] mSelection=multiSelectSpinner.getSelected();

                if(mSelection.length==0)
                {
                    SnackBarDisplay.showDefaultSnackBar(button,"Cannot proceed without selecting.");
                    return;
                }
                List<String> selection = new LinkedList<>();
                for (int i = 0; i < options.size(); ++i) {
                    if (mSelection[i]) {
                        selection.add(options.get(i));
                    }
                }
                String selected =joinString(", ",selection);
                Toast.makeText(SearchPage.this,selected,Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getBaseContext(), QuestionActivity.class);
                intent.putExtra(KEY_SELECTED_TAGS, selection.toArray());
                startActivity(intent);
            }
        });

    }

    private static String joinString(String joiner,List<String> stringList)
    {
        String returnString="";
        for (String item: stringList) {
            returnString+=item+joiner;
        }
        return returnString;
    }
}
