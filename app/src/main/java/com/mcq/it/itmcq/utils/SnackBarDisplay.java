package com.mcq.it.itmcq.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.Layout;
import android.view.View;

public class SnackBarDisplay {

    private  static View.OnClickListener defaultOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Snackbar snackbar1 = Snackbar.make(view, "Message is restored!", Snackbar.LENGTH_SHORT);
            snackbar1.show();
        }
    };

    public static void showDefaultSnackBar(View view, String message)
    {
        showSnackBar(view,message,"OK",defaultOnClickListener);
    }

    public static void showSnackBar(View view, String message, String actionMessage, View.OnClickListener customOnClick)
    {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setAction(actionMessage,customOnClick );
        snackbar.show();
    }

}
