package com.mcq.it.itmcq.parser;

import com.mcq.it.itmcq.exceptions.BadNetworkResponseException;
import com.mcq.it.itmcq.model.Question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class QuestionResponseParser {

    public List<Question> parseResponseQuestions(JSONArray rawArray) throws JSONException,BadNetworkResponseException
    {

        if((rawArray==null)) throw new BadNetworkResponseException("no question received from server");
        if(rawArray.length()==0) throw new BadNetworkResponseException("no question received from server");

        List<Question> questionList = new LinkedList<>();

        for(int index=0;index<rawArray.length();index++)
        {
            questionList.add(parseQuestion(rawArray.getJSONObject(index)));
        }
        return questionList;
    }

    private Question parseQuestion(JSONObject rawQuestion) throws JSONException,BadNetworkResponseException
    {
        //handle null value
        if(rawQuestion==null) throw new BadNetworkResponseException("corrupt question");

        Question newQuestion= new Question();
        newQuestion.setCorrectAnswer(rawQuestion.getString("correct_answer"));
        newQuestion.setQuestionString(rawQuestion.getString("questionString"));
        newQuestion.setTag(rawQuestion.getString("tag"));

        JSONArray rawIncorrectAnswer =rawQuestion.getJSONArray("options");

        List<String> incorrectAnswer = new LinkedList<>();

        //index through each incorrect answers list
        for(int index=0;index<rawIncorrectAnswer.length();index++)
        {
            incorrectAnswer.add(rawIncorrectAnswer.getString(index));
        }
        newQuestion.setIncorrectAnswers(incorrectAnswer);
        return newQuestion;
    }

}
